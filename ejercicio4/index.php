<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>
<body>
    <?php
        $a = 24.5;
        echo "El tipo de dato es ".gettype($a).'<br>';
        $a = "HOLA";
        echo "El tipo de dato es ".gettype($a).'<br>';
        settype($a, "integer");
        var_dump($a);
    ?>
</body>
</html>