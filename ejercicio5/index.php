<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css" type="text/css">
    <title>Ejercicio 5</title>
</head>
<body>
    <?php
        $num = 9;
        echo "<table>";
            echo "<tr>";
            echo "  <td class = 'title' colspan = '2'><h4>". "Tabla del " . $num . "</h4></td>";
            for($i = 1; $i <= $num; $i++) {
                if($i%2==0) {
                    echo "<tr>";
                    echo "  <td class = 'par'>" . "$num x $i" . "</td>";
                    echo "  <td class = 'par'>" .$num*$i. "</td>";
                    echo "</tr>"; 
                } else {
                    echo "<tr>";
                    echo "  <td class = 'inpar'>" . "$num x $i" ."</td>";
                    echo "  <td class = 'inpar'>" .$num*$i. "</td>";
                    echo "</tr>"; 
                }
            };
        echo "</table";
    ?>
</body>
</html>