<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>
<body>
    <?php
        $count = 0;
        while($count < 900) {
            $num = rand(1, 10000);
            if($num%2==0) {
                echo $num . "<br>";
                $count ++;
            }
        }
    ?>
</body>
</html>